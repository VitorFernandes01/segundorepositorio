<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/rest/MyRestControoller.php';

class SampleRest extends MyRestControoller {
    function __construct() {
        parent::__construct('sample');
    }

    function action_one_post(){
        $res = $this->model->action_one();
        $this->response($res, RESTController::HTTP_OK);
    }
    function action_two_get() {
        $res = $this->model->action_two();
        $this->response($res, RESTController::HTTP_OK);
    }

}