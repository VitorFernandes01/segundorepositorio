<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/rest/MyRestControoller.php';

class ContasRest extends MyRestControoller {
    function __construct() {
        parent::__construct('contas');
    }

    function delete_conta_post(){
        $res = $this->model->delete_one();
        $this->response($res, RESTController::HTTP_OK);
    }
    function status_conta_post() {
        $res = $this->model->status_conta();
        $this->response($res, RESTController::HTTP_OK);
    }

}