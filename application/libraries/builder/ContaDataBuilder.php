<?php
defined ('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/CI_Object.php';



    class ContaDataBuilder extends CI_Object {

    
        private $conta = 
        [
        [   'parceiro' => 'Magalu', 
            'descricao' => 'Notebook', 
           'valor' => '2000', 
            'mes' => '01', 
            'ano' => 2021, 
            'tipo' => 'pagar'
        ],
        [
            ''parceiro' => 'Casas Bahia, 
            'descricao' => 'Notebook', 
            'valor' => '3000', 
            'mes' => '01', 
            'ano' => 2021, 
            'tipo' => 'pagar'
        ],
        [   'parceiro' => 'Salário', 
            'descricao' => 'Prefeitura de Sucupira', 
            'valor' => '3542,18', 
            'mes' => '01', 
            'ano' => 2021, 
            'tipo' => 'receber'
        ],
        [      'parceiro' => 'Aluguel', 
            'descricao' => 'Casa Alugada', 
            'valor' => '680', 
            'mes' => '01', 
            'ano' => 2021, 
            'tipo' => 'receber'
        ],
        [   'parceiro' => 'Bandeirante', 
            'descricao' => 'energia elétrica', 
            'valor' => '97.25', 
            'mes' => '02', 
            'ano' => 2021, 
            'tipo' => 'pagar''
        ],
    ];
        
        public function start() {
            $this->load->library('conta');

            foreach ($contas as $conta) {
                $this->conta->cria($conta);
        }

        public function clear() {
            $this->db->truncate('conta');
    }
}