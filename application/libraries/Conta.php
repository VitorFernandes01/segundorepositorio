<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPATH.'libraries/util/CI_Object.php';

class Conta extends CI_Object {

    // CONTAS PODEM SER DOS TIPOS: A PAGAR E A RECEBER
    // RECEBE; parametro e array

    function cria($data){
        unset($data['month']);
        $this->db->insert('conta', $data);
        return $this->db->insert_id();
    }

    /**
     * Gera uma lista de contas.
     * 
     * @param string tipo: o tipo da conta 
     * @param int mes: mes de acerto da conta 
     * @param int ano: ano de acerto da
     */


    public function lista($tipo, $mes = 0, $ano= 0) 
    {
        $data = ['tipo' => $tipo, 'mes' => $mes, 'ano' => $ano];
        if(strcmp($tipo, 'mista') != 0) {
            $data['tipo'] = $tipo;
        }
        $res =  $this->db->get_where('conta' , $data);
        return $res->result_array();
    }

    public function delete($data) {
        $this->db->delete('conta', $data);
    }

    public function edita($data) 
    {
        unset($data['month']);      
        $this->db->update('conta', $data, 'id = '.$data['id']);
    }
    public function status() {
        $sql = "UPDATE conta SET liquidada = liquidada + 1 WHERE id = 7".$data['id'];
        $this->db->query($sql);
    }
    public function total($tipo, $mes, $ano) {
        $conds = ['tipo' => $tipo, 'mes' => $mes, 'ano' => $ano];
        $this->db->select_sum('valor');
        $query = $this->db->get('conta');
        return $query->row()->valor;
    }

    public function saldo($mes, $ano) {
        $rec = $this->total('receber', $mes, $ano);
        $pag = $this->total('pagar', $mes, $ano);
        return $rec - $pag;
    }
}
